package model

/**
  * Created by dicaraf on 16/03/2017.
  */
class Point(var x: Int, var y: Int) {
  def xCoordinate: Int = this.x

  def yCoordinate: Int = this.y

  def xCoordinate_(x: Int): Unit = this.x = x

  def yCoordinate_(y: Int): Unit = this.y = y
}