package model

import java.awt.Image

import controller.{MarioController, MushroomController, TurtleController}
import model.objects.{BasicObject, Flag, MagicMushroom, Piece}
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 12/04/2017.
  */
class Model {

  private val imgBackground1: Image = Utils.getImage(Res.IMG_BACKGROUND)
  private val imgBackground2: Image = Utils.getImage(Res.IMG_BACKGROUND)
  private val castle: Image = Utils.getImage(Res.IMG_CASTLE)
  private val start: Image = Utils.getImage(Res.START_ICON)
  private val imgCastle: Image = Utils.getImage(Res.IMG_CASTLE_FINAL)
  private var background1PosX: Int = Constants.INITIAL_POSITION_BACKGROUND
  private var background2PosX: Int = Constants.INITIAL_POSITION_BACKGROUND_2
  private var mov: Int = 0
  private var xPos: Int = Constants.INITIAL_POSITION_PLATFORM
  private var floorOffsetY: Int = Constants.FLOOR_OFFSET_Y_INITIAL
  private var heightLimit: Int = 0
  private var win: Boolean = false
  val CharactersFactory: EntityFactory = EntityFactoryProducer.getFactory(Constants.CHARACTER)
  private val mario: MarioController = CharactersFactory.getMario(new Point(Constants.MARIO_X, Constants.MARIO_Y))
  private val mushroom: MushroomController = CharactersFactory.getMushroom(new Point(Constants.MUSHROOM_X, Constants.MUSHROOM_Y))
  private val turtle: TurtleController = CharactersFactory.getTurtle(new Point(Constants.TURTLE_X, Constants.TURTLE_Y))
  val ObjectsFactory: EntityFactory = EntityFactoryProducer.getFactory(Constants.OBJECTS)
  private val flag: Flag = ObjectsFactory.getBasicObject(Constants.FLAG, Constants.FLAG_COORDINATE).asInstanceOf[Flag]
  private val magicmushroom: MagicMushroom = ObjectsFactory.getBasicObject(Constants.MAGICMUSHROOM, Constants.MAGICMUSHROOM_COORDINATE).asInstanceOf[MagicMushroom]
  private var objects : Seq[BasicObject] =IndexedSeq()
  objects = objects :+ this.flag
  var i: Int = 0
  while (i < Constants.BLOCKS_COORDINATES.size) {
    {
      objects = objects :+ ObjectsFactory.getBasicObject(Constants.BLOCK, Constants.BLOCKS_COORDINATES(i))
    }
    {
      i += 1
    }
  }
  i = 0
  while (i < Constants.TUNNEL_COORDINATES.size) {
    {
      objects = objects :+ ObjectsFactory.getBasicObject(Constants.TUNNEL, Constants.TUNNEL_COORDINATES(i))
    }
    {
      i += 1
    }
  }
  private var pieces : Seq[Piece] = IndexedSeq()
  i = 0
  while (i < Constants.PIECES_COORDINATES.size) {
    {
      pieces = pieces :+ ObjectsFactory.getPiece(Constants.PIECES_COORDINATES(i))
    }
    {
      i += 1
    }
  }

  def getMarioController: MarioController = mario

  def getMushroomController: MushroomController = mushroom

  def getTurtleController: TurtleController = turtle

  def getFlag: Flag = flag

  def getMagicMushroom: MagicMushroom =  magicmushroom

  def getObjects: Seq[BasicObject] = objects

  def getPieces: Seq[Piece] = pieces

  def setPieces(pieces: Seq[Piece]): Unit = this.pieces = pieces

  def getFloorOffsetY: Int = floorOffsetY

  def getHeightLimit: Int = heightLimit

  def getMov: Int = mov

  def getxPos: Int = xPos

  def getWin: Boolean =  win

  def setWin(win: Boolean) = this.win = win

  def getImgBackground1: Image = imgBackground1

  def getImgBackground2: Image = imgBackground2

  def imageCastle: Image = castle

  def imageCastleFinal: Image = imgCastle

  def imageStart: Image = start

  def getBackground1PosX: Int = background1PosX

  def getBackground2PosX: Int = background2PosX

  def setBackground2PosX(background2PosX: Int): Unit = this.background2PosX = background2PosX

  def setFloorOffsetY(floorOffsetY: Int): Unit = this.floorOffsetY = floorOffsetY

  def setHeightLimit(heightLimit: Int): Unit = this.heightLimit = heightLimit

  def setxPos(xPos: Int): Unit = this.xPos = xPos

  def setMov(mov: Int): Unit = this.mov = mov

  def setBackground1PosX(x: Int): Unit = this.background1PosX = x

}
