package model.characters

import java.awt.Image

import model.Point
import utils.Constants
import utils.Res
import utils.Utils

class Mario(coordinate: Point) extends BasicCharacter(coordinate, Constants.MARIO_WIDTH, Constants.MARIO_HEIGHT) {
  val imgMario: Image = Utils.getImage(Res.IMG_MARIO_DEFAULT)
  private var jumping: Boolean = false
  private var jumpingExtent: Int = 0
  private var immortal: Boolean = false
  private var sizeFactor: Double = 1
  private var immortalStart: Long = _

  def isJumping: Boolean = this.jumping

  def isJumping_(jumping: Boolean): Unit = this.jumping = jumping

  def isImmortal: Boolean = this.immortal

  def isImmortal_(immortal: Boolean): Unit = {
    this.immortal = immortal
    if(immortal){
      this.immortalStart = System.currentTimeMillis()
      this.sizeFactor = 1.15
    } else {
      this.sizeFactor = 1
    }
  }

  def finishedImmortal(): Unit = {
    if(System.currentTimeMillis() > this.immortalStart+Constants.IMMORTAL_TIME) this.isImmortal_(false)
  }

  def getSizeFactor: Double = this.sizeFactor

  def setJumpingExtent(jumpingExtent: Int): Unit = this.jumpingExtent = jumpingExtent

  def getJumpingExtent: Int = this.jumpingExtent

  def increaseJumpingExtent(): Unit = this.jumpingExtent + 1
}