package model.characters

import model.{BasicEntity, Point}

trait Character extends BasicEntity {
  def getCounter: Int

  def isAlive: Boolean

  def isMoving: Boolean

  def isMovingToRight: Boolean

  def isAlive_(alive: Boolean): Unit

  def isMoving_(moving: Boolean): Unit

  def isMovingToRight_(movingToRight: Boolean): Unit

  def setCounter(counter: Int): Unit
}

class BasicCharacter(coordinate: Point,
                     width: Int,
                     height: Int)
  extends BasicEntity(coordinate, width, height) with Character {

  private var moving: Boolean = false
  private var movingToRight: Boolean = true
  private var counter: Int = 0
  private var alive: Boolean = true

  def getCounter: Int = counter

  def increaseCounter: Int = this.counter + 1

  def isAlive: Boolean = alive

  def isMoving: Boolean = moving

  def isMovingToRight: Boolean = movingToRight

  def isAlive_(alive: Boolean): Unit = this.alive = alive

  def isMoving_(moving: Boolean): Unit = this.moving = moving

  def isMovingToRight_(movingToRight: Boolean): Unit = this.movingToRight = movingToRight

  def setCounter(counter: Int): Unit = this.counter = counter
}