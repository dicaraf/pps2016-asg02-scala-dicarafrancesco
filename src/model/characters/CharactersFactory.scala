package model.characters

import controller.MarioController
import controller.MushroomController
import controller.TurtleController
import model.{EntityFactory, Point}
import model.objects._

/**
  * Created by dicaraf on 18/03/2017.
  */
class CharactersFactory extends EntityFactory {
  def getMario(coordinate: Point): MarioController = new MarioController(coordinate)

  def getTurtle(coordinate: Point): TurtleController = new TurtleController(coordinate)

  def getMushroom(coordinate: Point): MushroomController = new MushroomController(coordinate)

  def getPiece(coordinate: Point): Piece = null

  def getBasicObject(name: String, Coordinate: Point): BasicObject = null
}