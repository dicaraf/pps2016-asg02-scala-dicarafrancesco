package model.characters

import java.awt.Image

import model.Point

/**
  * Created by dicaraf on 16/03/2017.
  */
trait Enemy extends Character {
  def getImage: Image
}

import utils.{Constants, Res, Utils}

class BasicEnemy(coordinate: Point,
                 width: Int,
                 height: Int)
  extends BasicCharacter(coordinate, width, height) with Enemy {
  this.isMovingToRight_(true)
  this.isMoving_(true)
  this.offsetX = Constants.LEFT
  private val img: Image = null
  private var offsetX: Int = 0

  //getters
  def getImage: Image = img

  def setOffsetX(offsetX: Int): Unit = this.offsetX = offsetX

  def getOffsetX: Int = offsetX
}
