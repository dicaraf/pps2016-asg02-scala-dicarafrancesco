package model.characters

import java.awt.Image

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 08/04/2017.
  */
class Mushroom(coordinate: Point) extends BasicEnemy(coordinate, Constants.MUSHROOM_WIDTH, Constants.MUSHROOM_HEIGHT) {
  val img: Image = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT)

  def deadImage: Image = Utils.getImage(if (this.isMovingToRight) Res.IMG_MUSHROOM_DEAD_DX
  else Res.IMG_MUSHROOM_DEAD_SX)
}