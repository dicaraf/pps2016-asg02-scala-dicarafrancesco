package model.characters

import java.awt.Image

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 08/04/2017.
  */
class Turtle(coordinate: Point) extends BasicEnemy(coordinate, Constants.TURTLE_WIDTH, Constants.TURTLE_HEIGHT) {
  val imgTurtle: Image = Utils.getImage(Res.IMG_TURTLE_IDLE)

  def deadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)
}
