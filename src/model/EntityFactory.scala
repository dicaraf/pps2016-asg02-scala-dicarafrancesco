package model

import controller.MarioController
import controller.MushroomController
import controller.TurtleController
import model.objects._

/**
  * Created by dicaraf on 17/03/2017.
  */
abstract class EntityFactory {
  def getMario(coordinate: Point): MarioController

  def getTurtle(coordinate: Point): TurtleController

  def getMushroom(coordinate: Point): MushroomController

  def getPiece(coordinate: Point): Piece

  def getBasicObject(name: String, Coordinate: Point): BasicObject
}