package model

/**
  * Created by dicaraf on 07/04/2017.
  */
trait Entity {
  def xCoordinate: Int

  def yCoordinate: Int

  def getWidth: Int

  def getHeight: Int

  def xCoordinate_(x: Int): Unit

  def yCoordinate_(y: Int): Unit

  def moveOnSceneChange()
}

import game.Main

class BasicEntity protected(var coordinate: Point, val width: Int, val height: Int) extends Entity {

  def xCoordinate: Int = this.coordinate.xCoordinate

  def yCoordinate: Int = this.coordinate.yCoordinate

  def getWidth: Int = width

  def getHeight: Int = height

  def xCoordinate_(x: Int): Unit = this.coordinate.xCoordinate_(x)

  def yCoordinate_(y: Int):Unit = this.coordinate.yCoordinate_(y)

  def moveOnSceneChange() {
    if (Main.getScene.getModel.getxPos >= 0) {
      this.xCoordinate_(this.xCoordinate - Main.getScene.getModel.getMov)
    }
  }
}