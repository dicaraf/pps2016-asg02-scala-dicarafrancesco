package model.objects

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 12/04/2017.
  */
class MagicMushroom(coordinate: Point)
  extends BasicObject(coordinate, Constants.MAGICMUSHROOM_WIDTH, Constants.MAGICMUSHROOM_HEIGHT) {

  private var visible = true

  super.image_(Utils.getImage(Res.IMG_MAGICMUSHROOM))

  def isVisible(): Boolean = this.visible

  def isVisible_(visible: Boolean): Unit = this.visible = visible
}
