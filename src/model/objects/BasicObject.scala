package model.objects

import java.awt.Image

import model.BasicEntity
import model.Point

class BasicObject(coordinate: Point,
                  width: Int,
                  height: Int)
  extends BasicEntity(coordinate, width, height) {

  private var imgObj: Image = _

  def image: Image = imgObj

  def image_(imgObj: Image): Unit = this.imgObj = imgObj

}
