package model.objects

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 08/04/2017.
  */
class Block(coordinate: Point)
  extends BasicObject(coordinate, Constants.BLOCK_WIDTH, Constants.BLOCK_HEIGHT) {

  super.image_(Utils.getImage(Res.IMG_BLOCK))
}
