package model.objects

import java.awt.Image

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 08/04/2017.
  */
class Piece(coordinate: Point)
  extends BasicObject(coordinate, Constants.PIECE_WIDTH, Constants.PIECE_HEIGHT) with Runnable {

  super.image_(Utils.getImage(Res.IMG_PIECE1))
  private var counter: Int = 0

  def imageOnMovement: Image = {
    Utils.getImage(if ( {
      this.counter += 1; this.counter
    } % Constants.FLIP_FREQUENCY == 0) Res.IMG_PIECE1
    else Res.IMG_PIECE2)
  }

  def run() {
    while (true) {
      {
        this.imageOnMovement
        try {
          Thread.sleep(Constants.PIECE_PAUSE)
        }
        catch {
          case e: InterruptedException => {
            e.printStackTrace()
          }
        }
      }
    }
  }
}
