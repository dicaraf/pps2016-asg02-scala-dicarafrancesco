package model.objects

import controller.MarioController
import controller.MushroomController
import controller.TurtleController
import model._
import utils.Constants

/**
  * Created by dicaraf on 18/03/2017.
  */
class ObjectsFactory extends EntityFactory {
  def getMario(coordinate: Point): MarioController = null

  def getTurtle(coordinate: Point): TurtleController = null

  def getMushroom(coordinate: Point): MushroomController = null

  def getPiece(coordinate: Point): Piece = new Piece(coordinate)



  def getBasicObject(name: String, coordinate: Point): BasicObject = {

    if (name.equalsIgnoreCase(Constants.BLOCK)) {
      new Block(coordinate)
    }
    else if (name.equalsIgnoreCase(Constants.TUNNEL)) {
      new Tunnel(coordinate)
    }
    else if (name.equalsIgnoreCase(Constants.FLAG)){
        new Flag(coordinate)
    }
    else if (name.equalsIgnoreCase(Constants.MAGICMUSHROOM)){
      new MagicMushroom(coordinate)
    }
    else {
      null
    }

  }
}