package model.objects

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 08/04/2017.
  */
class Tunnel(coordinate: Point)
  extends BasicObject(coordinate, Constants.TUNNEL_WIDTH, Constants.TUNNEL_HEIGHT) {

  super.image_(Utils.getImage(Res.IMG_TUNNEL))
}
