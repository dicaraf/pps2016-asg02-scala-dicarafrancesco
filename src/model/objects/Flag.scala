package model.objects

import model.Point
import utils.{Constants, Res, Utils}

/**
  * Created by dicaraf on 11/04/2017.
  */
class Flag (coordinate: Point)
  extends BasicObject(coordinate, Constants.FLAG_WIDTH, Constants.FLAG_HEIGHT) {

    super.image_(Utils.getImage(Res.IMG_FLAG))

}
