package model

import model.characters.CharactersFactory
import model.objects.ObjectsFactory
import utils.Constants

/**
  * Created by dicaraf on 17/03/2017.
  */
object EntityFactoryProducer {
  def getFactory(choice: String): EntityFactory = {
    if (choice.equalsIgnoreCase(Constants.OBJECTS)) {
      return new ObjectsFactory()
    }
    else if (choice.equalsIgnoreCase(Constants.CHARACTER)) {
      return new CharactersFactory()
    }
    null
  }
}