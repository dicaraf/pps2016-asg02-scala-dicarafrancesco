package controller

import model.characters.BasicCharacter
import model.characters.Mario
import model.objects.{BasicObject, MagicMushroom, Piece}
import model.Point
import game.Main
import utils.Constants
import utils.Res
import utils.Utils
import java.awt._

/**
  * Created by dicaraf on 16/03/2017.
  */
class MarioController(val coordinate: Point) extends BasicCharacterController {
  this.mario = new Mario(coordinate)
  private var mario: Mario = _

  def getMario: Mario = this.mario

  def doJump: Image = {
    var str: String = null
    this.mario.increaseJumpingExtent()
    if (this.mario.getJumpingExtent < Constants.MARIO_JUMPING_LIMIT) {
      if (this.mario.yCoordinate > Main.getScene.getModel.getHeightLimit) {
        this.mario.yCoordinate_(this.mario.yCoordinate - 4)
      }
      else {
        this.mario.setJumpingExtent(Constants.MARIO_JUMPING_LIMIT)
      }
      str = if (this.mario.isMovingToRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else if (this.mario.yCoordinate + this.mario.getHeight < Main.getScene.getModel.getFloorOffsetY) {
      this.mario.yCoordinate_(this.mario.yCoordinate + 1)
      str = if (this.mario.isMovingToRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else {
      str = if (this.mario.isMovingToRight) Res.IMG_MARIO_ACTIVE_DX
      else Res.IMG_MARIO_ACTIVE_SX
      this.mario.isJumping_(false)
      this.mario.setJumpingExtent(0)
    }
    Utils.getImage(str)
  }

  def contact(obj: BasicObject) {
    if (this.hitAhead(this.mario, obj) && this.mario.isMovingToRight || this.hitBack(this.mario, obj) && !this.mario.isMovingToRight) {
      Main.getScene.getModel.setMov(0)
      this.mario.isMoving_(false)
    }
    if (this.hitBelow(this.mario, obj) && this.mario.isJumping) {
      Main.getScene.getModel.setFloorOffsetY(obj.yCoordinate)
    }
    else if (!this.hitBelow(this.mario, obj)) {
      Main.getScene.getModel.setFloorOffsetY(Constants.FLOOR_OFFSET_Y_INITIAL)
      if (!this.mario.isJumping) {
        this.mario.yCoordinate_(Constants.MARIO_OFFSET_Y_INITIAL)
      }
      if (hitAbove(this.mario, obj)) {
        Main.getScene.getModel.setHeightLimit(obj.yCoordinate + obj.getHeight) // the new sky goes below the object
      }
      else if (!this.hitAbove(this.mario, obj) && !this.mario.isJumping) {
        Main.getScene.getModel.setHeightLimit(0) // initial sky
      }
    }
  }

  def contactPiece(piece: Piece): Boolean = {
    this.hitBack(this.mario, piece) || this.hitAbove(this.mario, piece) || this.hitAhead(this.mario, piece) || this.hitBelow(this.mario, piece)
  }

  def contactMagicMushroom(magicMushroom: MagicMushroom): Boolean = {
    this.hitBack(this.mario, magicMushroom) || this.hitAbove(this.mario, magicMushroom) || this.hitAhead(this.mario, magicMushroom) || this.hitBelow(this.mario, magicMushroom)
  }

  def contact(character: BasicCharacter) {
    if (this.hitAhead(this.mario, character) || this.hitBack(this.mario, character)) {
      if (character.isAlive) {
        this.mario.isMoving_(false)
        this.mario.isAlive_(false)
      }
      else {
        this.mario.isAlive_(true)
      }
    }
    else if (this.hitBelow(this.mario, character)) {
      character.isMoving_(false)
      character.isAlive_(false)
    }
  }
}