package controller

import game.{Audio, Keyboard}
import model.Model
import utils.{Constants, Res}

/**
  * Created by dicaraf on 12/04/2017.
  */
class PlatformController(val model: Model) {

  def updateBackgroundOnMovement() {
    if (model.getxPos >= 0 && model.getxPos <= Constants.FINAL_POSITION_PLATFORM) {
      model.setxPos(model.getxPos + model.getMov)
      // Moving the screen to give the impression that  model.getMarioController is walking
      model.setBackground1PosX (model.getBackground1PosX - model.getMov)
      model.setBackground2PosX (model.getBackground2PosX - model.getMov)
    }
    // Flipping between background1 and background2
    if (model.getBackground1PosX == -Constants.FLIPPING_BACKGROUND_X) {
      model.setBackground1PosX (Constants.FLIPPING_BACKGROUND_X)
    }
    else if (model.getBackground2PosX == -Constants.FLIPPING_BACKGROUND_X) {
      model.setBackground2PosX (Constants.FLIPPING_BACKGROUND_X)
    }
    else if (model.getBackground1PosX == Constants.FLIPPING_BACKGROUND_X) {
      model.setBackground1PosX (-Constants.FLIPPING_BACKGROUND_X)
    }
    else if (model.getBackground2PosX == Constants.FLIPPING_BACKGROUND_X) {
      model.setBackground2PosX ( -Constants.FLIPPING_BACKGROUND_X)
    }
  }
  
  def checkContact(): Unit = {
    for (objectInstance <- model.getObjects) {
      if ( model.getMarioController.isNearby( model.getMarioController.getMario, objectInstance)) {
         model.getMarioController.contact(objectInstance)
      }
      if ( model.getMushroomController.isNearby( model.getMushroomController.getMushroom, objectInstance)) {
         model.getMushroomController.contact(objectInstance)
      }
      if ( model.getTurtleController.isNearby( model.getTurtleController.getTurtle, objectInstance)) {
         model.getTurtleController.contact(objectInstance)
      }
    }
    for (pieceInstance <- model.getPieces) {
      if ( model.getMarioController.contactPiece(pieceInstance)) {
        Audio.playSound(Res.AUDIO_MONEY)
        model.setPieces(model.getPieces.take(model.getPieces.indexOf(pieceInstance)) ++ model.getPieces.drop(model.getPieces.indexOf(pieceInstance) + 1))
      }
    }
    if ( model.getMushroomController.isNearby( model.getMushroomController.getMushroom,  model.getTurtleController.getTurtle)) {
       model.getMushroomController.contact( model.getTurtleController.getTurtle)
    }
    if ( model.getTurtleController.isNearby( model.getTurtleController.getTurtle,  model.getMushroomController.getMushroom)) {
       model.getTurtleController.contact( model.getMushroomController.getMushroom)
    }
    if(! model.getMarioController.getMario.isImmortal){
      if ( model.getMarioController.isNearby( model.getMarioController.getMario,  model.getMushroomController.getMushroom)) {
         model.getMarioController.contact( model.getMushroomController.getMushroom)
      }
      if ( model.getMarioController.isNearby( model.getMarioController.getMario,  model.getTurtleController.getTurtle)) {
         model.getMarioController.contact( model.getTurtleController.getTurtle)
      }
    }
    if ( model.getMarioController.isNearby( model.getMarioController.getMario, model.getFlag)) {
      if(model.getPieces.isEmpty)  model.setWin(true)
    }
    if ( model.getMarioController.isNearby( model.getMarioController.getMario,  model.getMagicMushroom)){
      if( model.getMarioController.contactMagicMushroom( model.getMagicMushroom)){
         model.getMarioController.getMario.isImmortal_(true)
         model.getMagicMushroom.isVisible_(false)
      }
    }
    // Moving fixed objects

    updateBackgroundOnMovement()
    if (model.getxPos >= 0 &&  model.getxPos <= Constants.FINAL_POSITION_PLATFORM) {
      for (objectInstance <-  model.getObjects) {
        objectInstance.moveOnSceneChange()
      }
      for (pieceInstance <-  model.getPieces) {
        pieceInstance.moveOnSceneChange()
      }
       model.getMagicMushroom.moveOnSceneChange()
       model.getMushroomController.moveOnSceneChange()
       model.getTurtleController.moveOnSceneChange()
    }
  }

}
