package controller

import model._
import model.characters.BasicCharacter
import utils.Constants
import utils.Res
import utils.Utils
import java.awt._

/**
  * Created by dicaraf on 16/03/2017.
  */
class BasicCharacterController private[controller]() {
  def walk(character: BasicCharacter, name: String, frequency: Int): Image = {
    val str: String = Res.IMG_BASE + name + (if (!character.isMoving || character.increaseCounter % frequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (character.isMovingToRight) Res.IMGP_DIRECTION_DX
    else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
    Utils.getImage(str)
  }

  private[controller] def hitAhead(character: BasicCharacter, entity: BasicEntity): Boolean = {
    !(character.xCoordinate + character.getWidth < entity.xCoordinate || character.xCoordinate + character.getWidth > entity.xCoordinate + Constants.HIT_MARGIN || character.yCoordinate + character.getHeight <= entity.yCoordinate || character.yCoordinate >= entity.yCoordinate + entity.getHeight)
  }

  private[controller] def hitBack(character: BasicCharacter, entity: BasicEntity): Boolean = {
    !(character.xCoordinate > entity.xCoordinate + entity.getWidth || character.xCoordinate + character.getWidth < entity.xCoordinate + entity.getWidth - Constants.HIT_MARGIN || character.yCoordinate + character.getHeight <= entity.yCoordinate || character.yCoordinate >= entity.yCoordinate + entity.getHeight)
  }

  private[controller] def hitBelow(character: BasicCharacter, entity: BasicEntity): Boolean = {
    !(character.xCoordinate + character.getWidth < entity.xCoordinate + Constants.HIT_MARGIN || character.xCoordinate > entity.xCoordinate + entity.getWidth - Constants.HIT_MARGIN || character.yCoordinate + character.getHeight < entity.yCoordinate || character.yCoordinate + character.getHeight > entity.yCoordinate + Constants.HIT_MARGIN)
  }

  private[controller] def hitAbove(character: BasicCharacter, entity: BasicEntity): Boolean = {
    !(character.xCoordinate + character.getWidth < entity.xCoordinate + Constants.HIT_MARGIN || character.xCoordinate > entity.xCoordinate + entity.getWidth - Constants.HIT_MARGIN || character.yCoordinate < entity.yCoordinate + entity.getHeight || character.yCoordinate > entity.yCoordinate + entity.getHeight + Constants.HIT_MARGIN)
  }

  def isNearby(character: BasicCharacter, entity: BasicEntity): Boolean = {
    (character.xCoordinate > entity.xCoordinate - Constants.PROXIMITY_MARGIN && character.xCoordinate < entity.xCoordinate + entity.getWidth + Constants.PROXIMITY_MARGIN) || (character.xCoordinate + character.getWidth > entity.xCoordinate - Constants.PROXIMITY_MARGIN && character.xCoordinate + character.getWidth < entity.xCoordinate + entity.getWidth + Constants.PROXIMITY_MARGIN)
  }
}