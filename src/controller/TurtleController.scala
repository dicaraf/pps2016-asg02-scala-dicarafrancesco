package controller

import model.BasicEntity
import model.Point
import model.characters.Turtle
import utils.Constants

/**
  * Created by dicaraf on 16/03/2017.
  */
class TurtleController(val coordinateTurtle: Point) extends BasicCharacterController with Runnable {
  turtle = new Turtle(coordinateTurtle)
  val enemy: Thread = new Thread(this)
  enemy.start()
  private var turtle: Turtle = _

  def getTurtle: Turtle = this.turtle

  def moveOnSceneChange() {
    this.turtle.setOffsetX(if (this.turtle.isMovingToRight) Constants.LEFT
    else Constants.RIGHT)
    this.turtle.xCoordinate_(this.turtle.xCoordinate + this.turtle.getOffsetX)
  }

  def run() {
    while (true) {
      {
        if (this.turtle.isAlive) {
          this.moveOnSceneChange()
          try {
            Thread.sleep(Constants.ENEMY_PAUSE)
          }
          catch {
            case e: InterruptedException =>
              e.printStackTrace()
          }
        }
      }
    }
  }

  def contact(entity: BasicEntity) {
    if (this.hitAhead(this.turtle, entity) && this.turtle.isMovingToRight) {
      this.turtle.isMovingToRight_(false)
      this.turtle.setOffsetX(Constants.RIGHT)
    }
    else if (this.hitBack(this.turtle, entity) && !this.turtle.isMovingToRight) {
      this.turtle.isMovingToRight_(true)
      this.turtle.setOffsetX(Constants.LEFT)
    }
  }
}