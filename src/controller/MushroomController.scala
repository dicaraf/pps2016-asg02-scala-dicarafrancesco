package controller

import model.BasicEntity
import model.Point
import model.characters.Mushroom
import utils.Constants

/**
  * Created by dicaraf on 16/03/2017.
  */
class MushroomController(val coordinate: Point) extends BasicCharacterController with Runnable {
  mushroom = new Mushroom(coordinate)
  val enemy: Thread = new Thread(this)
  enemy.start()
  private var mushroom: Mushroom = _

  def getMushroom: Mushroom = this.mushroom

  def moveOnSceneChange() {
    this.mushroom.setOffsetX(if (this.mushroom.isMovingToRight) Constants.LEFT
    else Constants.RIGHT)
    this.mushroom.xCoordinate_(this.mushroom.xCoordinate + this.mushroom.getOffsetX)
  }

  def run() {
    while (true) {
      {
        if (this.mushroom.isAlive) {
          this.moveOnSceneChange()
          try {
            Thread.sleep(Constants.ENEMY_PAUSE)
          }
          catch {
            case e: InterruptedException => {
              e.printStackTrace()
            }
          }
        }
      }
    }
  }

  def contact(entity: BasicEntity) {
    if (this.hitAhead(this.mushroom, entity) && this.mushroom.isMovingToRight) {
      this.mushroom.isMovingToRight_(false)
      this.mushroom.setOffsetX(Constants.RIGHT)
    }
    else if (this.hitBack(this.mushroom, entity) && !this.mushroom.isMovingToRight) {
      this.mushroom.isMovingToRight_(true)
      this.mushroom.setOffsetX(Constants.LEFT)
    }
  }
}