package game;

import utils.Constants;

import javax.swing.*;

public class Refresh implements Runnable {

    public void run() {
        while (Main.getScene().getModel().getMarioController().getMario().isAlive() && !Main.getScene().getModel().getWin()) {
            Main.getScene().repaint();
            Main.getScene().getModel().getMarioController().getMario().finishedImmortal();
            try {
                Thread.sleep(Constants.PAUSE);
            } catch (InterruptedException e) {
            }
        }

        if(Main.getScene().getModel().getWin()){
            JOptionPane.showMessageDialog(Main.getScene(),
                    "You win!",
                    "Congratulations!",
                    JOptionPane.PLAIN_MESSAGE);
            System.exit(0);
        } else if(!Main.getScene().getModel().getMarioController().getMario().isAlive()){
            JOptionPane.showMessageDialog(Main.getScene(),
                    "Game Over",
                    "Game Over",
                    JOptionPane.PLAIN_MESSAGE);
            System.exit(0);
        }

        /* BUG: quando si crea il nuovo gioco gli oggetti sono fuori posizione

        if(Main.getScene().getModel().getWin()){
            int popup = JOptionPane.showConfirmDialog(Main.getScene(),
                    "You win. Play again?",
                    "You win!!",
                    JOptionPane.YES_NO_OPTION);
            if(popup == JOptionPane.YES_OPTION) {
                Main.newGame();
            } else{
                System.exit(0);
            }
        } else if(!Main.getScene().getModel().getMarioController().getMario().isAlive()){
            int popup = JOptionPane.showConfirmDialog(Main.getScene(),
                    "Game Over. Play again?",
                    "Game Over",
                    JOptionPane.YES_NO_OPTION);
            if(popup == JOptionPane.YES_OPTION) {
                Main.newGame();
            } else{
                System.exit(0);
            }

        }*/

    }

} 
