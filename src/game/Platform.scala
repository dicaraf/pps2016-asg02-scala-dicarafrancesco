package game

import controller.PlatformController
import model.Model
import utils.Constants
import utils.Res
import javax.swing._
import java.awt._

class Platform private[game]() extends JPanel {
  private val model: Model = new Model
  private val controller: PlatformController = new PlatformController(model)

  this.setFocusable(true)
  this.requestFocusInWindow
  this.addKeyListener(new Keyboard)

  def getController: PlatformController = controller

  def getModel: Model = model

  override def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)

    controller.checkContact();

    g.drawImage(model.getImgBackground1, model.getBackground1PosX, Constants.INITIAL_POSITION_BACKGROUND_Y, null)
    g.drawImage(model.getImgBackground2, model.getBackground2PosX, Constants.INITIAL_POSITION_BACKGROUND_Y, null)
    g.drawImage(model.imageCastle, Constants.INITIAL_POSITION_CASTLE_X - model.getxPos, Constants.INITIAL_POSITION_CASTLE_Y, null)
    g.drawImage(model.imageStart, Constants.INITIAL_POSITION_START_X - model.getxPos, Constants.INITIAL_POSITION_START_Y, null)
    if(model.getMagicMushroom.isVisible()) {
      g.drawImage(model.getMagicMushroom.image, model.getMagicMushroom.xCoordinate, model.getMagicMushroom.yCoordinate, null)
    }

    var i: Int = 0
    for (objectInstance <- model.getObjects) {
        g.drawImage(objectInstance.image, objectInstance.xCoordinate, objectInstance.yCoordinate, null)
    }
    for (pieceInstance <- model.getPieces) {
      {
        g.drawImage(pieceInstance.imageOnMovement, pieceInstance.xCoordinate, pieceInstance.yCoordinate, null)
      }
      {
        i += 1
      }
    }
    g.drawImage(model.imageCastleFinal, Constants.CASTLE_X_POS - model.getxPos, Constants.CASTLE_Y_POS, null)
    if (model.getMarioController.getMario.isJumping) {
      g.drawImage(model.getMarioController.doJump, model.getMarioController.getMario.xCoordinate, model.getMarioController.getMario.yCoordinate, (Constants.MARIO_WIDTH*model.getMarioController.getMario.getSizeFactor).toInt, (Constants.MARIO_HEIGHT*model.getMarioController.getMario.getSizeFactor).toInt, null)
    }
    else {
      g.drawImage(model.getMarioController.walk(model.getMarioController.getMario, Res.IMGP_CHARACTER_MARIO, Constants.MARIO_FREQUENCY), model.getMarioController.getMario.xCoordinate, model.getMarioController.getMario.yCoordinate, (Constants.MARIO_WIDTH*model.getMarioController.getMario.getSizeFactor).toInt, (Constants.MARIO_HEIGHT*model.getMarioController.getMario.getSizeFactor).toInt, null)
    }
    if (model.getMushroomController.getMushroom.isAlive) {
      g.drawImage(model.getMushroomController.walk(model.getMushroomController.getMushroom, Res.IMGP_CHARACTER_MUSHROOM, Constants.MUSHROOM_FREQUENCY), model.getMushroomController.getMushroom.xCoordinate, model.getMushroomController.getMushroom.yCoordinate, null)
    }
    else {
      g.drawImage(model.getMushroomController.getMushroom.deadImage, model.getMushroomController.getMushroom.xCoordinate, model.getMushroomController.getMushroom.yCoordinate + Constants.MUSHROOM_DEAD_OFFSET_Y, null)
    }
    if (model.getTurtleController.getTurtle.isAlive) {
      g.drawImage(model.getTurtleController.walk(model.getTurtleController.getTurtle, Res.IMGP_CHARACTER_TURTLE, Constants.TURTLE_FREQUENCY), model.getTurtleController.getTurtle.xCoordinate, model.getTurtleController.getTurtle.yCoordinate, null)
    }
    else {
      g.drawImage(model.getTurtleController.getTurtle.deadImage, model.getTurtleController.getTurtle.xCoordinate, model.getTurtleController.getTurtle.yCoordinate + Constants.TURTLE_DEAD_OFFSET_Y, null)
    }
  }
}