package game

import utils.Constants
import javax.swing.JFrame

object Main extends App{
  private var scene: Platform = _

    val window: JFrame = new JFrame(Constants.WINDOW_TITLE)
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    window.setSize(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT)
    window.setLocationRelativeTo(null)
    window.setResizable(true)
    window.setAlwaysOnTop(true)
    newGame

  def newGame(): Unit = {
    scene = new Platform
    window.setContentPane(scene)
    window.setVisible(true)
    val timer: Thread = new Thread(new Refresh)
    timer.start()
  }

  def getScene: Platform =  scene
}