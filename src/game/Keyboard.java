package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.getScene().getModel().getMarioController().getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.getScene().getModel().getxPos() == -1) {
                    Main.getScene().getModel().setxPos(0);
                    Main.getScene().getModel().setBackground1PosX(-50);
                    Main.getScene().getModel().setBackground2PosX(750);
                }
                Main.getScene().getModel().getMarioController().getMario().isMoving_(true);
                Main.getScene().getModel().getMarioController().getMario().isMovingToRight_(true);
                Main.getScene().getModel().setMov(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.getScene().getModel().getxPos() == 4601) {
                    Main.getScene().getModel().setxPos(4600);
                    Main.getScene().getModel().setBackground1PosX(-50);
                    Main.getScene().getModel().setBackground2PosX(750);
                }

                Main.getScene().getModel().getMarioController().getMario().isMoving_(true);
                Main.getScene().getModel().getMarioController().getMario().isMovingToRight_(false);
                Main.getScene().getModel().setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.getScene().getModel().getMarioController().getMario().isJumping_(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.getScene().getModel().getMarioController().getMario().isMoving_(false);
        Main.getScene().getModel().setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
